import { reactive } from "vue";

type TChannelInfoState = {
  [key: string]: { channelName: string; channelArn: string };
};

export default {
  state: reactive<TChannelInfoState>({}),
  setChannelInfo(channelId: string, channelArn: string, channelName: string) {
    this.state[channelId] = { channelArn, channelName };
  },
};
