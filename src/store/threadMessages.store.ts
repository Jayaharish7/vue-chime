import { IChannelMessage, TChannelMessages } from "@/types/channel.types";
import { reactive } from "vue";

export default {
  state: reactive<TChannelMessages>({}),

  addThread(messageId: string, data?: IChannelMessage[]) {
    this.state[messageId] = data ?? [];
  },
  addThreadMessage(messageId: string, ...message: IChannelMessage[]) {
    if (!this.state[messageId]) this.state[messageId] = [];
    this.state[messageId].push(...message);
  },
};
