import { IChannelMessage, TChannelMessages } from "@/types/channel.types";
import { reactive } from "vue";

export default {
  state: reactive<TChannelMessages>({}),

  addChannel(channelId: string, data?: IChannelMessage[]) {
    this.state[channelId] = data ?? [];
  },
  addChannelMessage(channelId: string, ...message: IChannelMessage[]) {
    if (!this.state[channelId]) this.state[channelId] = [];
    this.state[channelId].push(...message);
  },
};
