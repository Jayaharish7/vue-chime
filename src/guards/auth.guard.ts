import Auth from "@aws-amplify/auth";
import { NavigationGuard } from "vue-router";

export const AuthGuard: NavigationGuard = async (from, to, next) => {
  try {
    await Auth.currentAuthenticatedUser();
    next();
  } catch (err) {
    next({ name: "LOGIN" });
  }
};
export const UnAuthGuard: NavigationGuard = async (from, to, next) => {
  try {
    await Auth.currentAuthenticatedUser();
    next({ name: "HOME" });
  } catch (err) {
    next();
  }
};
