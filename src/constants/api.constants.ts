export const API_ROUTES = {
    LIST_USER_CHANNELS: 'listChannelMembershipForAppinstanceUser',
    CREATE_CHANNEL: 'createChannel',
    SEND_CHANNEL_MESSAGE: 'sendChannelMessage',
    CREATE_CHANNEL_MEMBERSHIP: 'createChannelMembership',
    LIST_CHANNEL_MESSAGE: 'listChannelMessage',
    DESCRIBE_CHANNEL: 'describeChannel',
    GET_MESSAGING_SESSION_ENDPOINT: 'getMessagingSessionEndpoint',
    SEND_THREAD_MESSAGE: 'sendThreadMessage',
    UPDATE_CHANNEL: 'updateChannel'
}