import { MESSAGE_TYPE } from "@/constants/app.constants";
import { MESSAGE_VARIANT } from "@/constants/channel.constants";
import {
  IChannelMessage,
  ICreateMessageEventPayload,
} from "@/types/channel.types";

export const convertArnToChannelId = (channelArn: string) => {
  return channelArn.split("/").slice(3, 4)[0];
};

export const getMessageVariantDetails = (
  message: IChannelMessage | ICreateMessageEventPayload
) => {
  const metadata = message.Metadata;
  const { AppMessageVariant, ReplyMessageId } = JSON.parse(metadata ?? "{}");
  if (AppMessageVariant === MESSAGE_VARIANT.THREAD_MESSAGE)
    return {
      AppMessageVariant: MESSAGE_VARIANT.THREAD_MESSAGE,
      ReplyMessageId: ReplyMessageId as string,
    };
  return { AppMessageVariant: MESSAGE_VARIANT.STANDARD };
};
