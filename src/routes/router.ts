import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../page/Home.vue";
import Login from "../page/Login.vue";

import Room from "../components/Room.vue";
import CreateChatRoom from "../components/CreateChatRoom.vue";
import AddUserToChannel from "../components/AddUserToChannel.vue";
import DefaultView from "../components/DefaultView.vue";
import { AuthGuard, UnAuthGuard } from "@/guards/auth.guard";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: Home,
    beforeEnter: [AuthGuard],
    children: [
      { path: "", component: DefaultView, name: "HOME" },
      { path: "room/create", component: CreateChatRoom, name: "CREATE_ROOM" },
      { path: "room/:id/add", component: AddUserToChannel, name: "ADD_USER" },
      { path: "room/:id", component: Room, name: "ROOM" },
    ],
  },
  {
    path: "/login",
    component: Login,
    name: "LOGIN",
    beforeEnter: [UnAuthGuard],
  },
  { path: "/:catchAll(.*)", redirect: "/login" },
];

export const Router = createRouter({
  history: createWebHistory(),
  routes,
});
