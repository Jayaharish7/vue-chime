import { environment } from "./environment/environment";
export const AWS_EXPORTS = {
  Auth: {
    region: environment.region,
    userPoolId: environment.userPoolId,
    userPoolWebClientId: environment.userPoolWebClientId,
    identityPoolId: environment.identityPoolId,
  },
  API: {
    endpoints: [
      {
        name: environment.apiName,
        endpoint: environment.baseUrl,
        region: environment.region,
      },
    ],
  },
};
