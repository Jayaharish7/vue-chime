import { Auth } from "aws-amplify";
import { useRoute } from "vue-router";
export class AuthService {
  static async login(username: string, password: string) {
    const user = await Auth.signIn(username, password);
    return user;
  }
  static async changePassword(user: any, newPassword: string) {
    const response = await Auth.completeNewPassword(user, newPassword);
    return response;
  }
  static async currentUserSub() {
    const response = await Auth.currentUserInfo();
    return response.attributes.sub;
  }
  static currentUser() {
    return Auth.currentUserInfo();
  }
}
