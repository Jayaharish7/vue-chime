import { API_ROUTES } from "@/constants/api.constants";
import { APP_INSTANCE_ARN } from "@/constants/app.constants";
import {
  APP_CHANNEL_TYPE,
  MESSAGE_VARIANT,
} from "@/constants/channel.constants";
import { environment } from "@/environment/environment";
import { IListChannelMessageResponse } from "@/types/channel.types";
import { getMessageVariantDetails } from "@/utils/common.util";
import API from "@aws-amplify/api";
import Auth from "@aws-amplify/auth";
import {
  ChannelMessagePersistenceType,
  ChannelMode,
  ChannelPrivacy,
  CreateChannelMembershipResponse,
  CreateChannelResponse,
  DescribeChannelResponse,
  GetMessagingSessionEndpointResponse,
  ListChannelMembershipsForAppInstanceUserResponse,
  ListChannelMessagesResponse,
  SendChannelMessageResponse,
  UpdateChannelResponse,
} from "@aws-sdk/client-chime";
import {
  ConsoleLogger,
  DefaultMessagingSession,
  LogLevel,
  MessagingSessionConfiguration,
} from "amazon-chime-sdk-js";
import AWS from "aws-sdk";
import AWSChime from "aws-sdk/clients/chime";
import { SendMessageResponse } from "aws-sdk/clients/connectparticipant";
import { v4 as uuidv4 } from "uuid";
import { AuthService } from "./auth.service";

export class ChimeService {
  static chimeClient: AWSChime;
  static awsChime: AWSChime;
  static chimeBearer: string;
  static async getClient() {
    if (!this.chimeClient) {
      const credentials = await Auth.currentCredentials().then((data) => data);
      this.chimeClient = await new AWSChime({
        region: "us-east-1",
        credentials,
      });
    }
    return this.chimeClient;
  }
  static async sendMessage(
    channelId: string,
    message: string,
    metadata?: string,
    arn: boolean = false
  ) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.SEND_CHANNEL_MESSAGE;
    const response: SendChannelMessageResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          userId: await AuthService.currentUserSub(),
          channelArn,
          messageContent: message,
          persistence: ChannelMessagePersistenceType.PERSISTENT,
          metadata,
        },
      }
    );
    return response;
  }
  static async createChannel(
    name: string,
    privacy: string | ChannelPrivacy,
    type: APP_CHANNEL_TYPE = APP_CHANNEL_TYPE.STANDARD
  ) {
    const path = API_ROUTES.CREATE_CHANNEL;
    const response: CreateChannelResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          name,
          privacy,
          mode: ChannelMode.UNRESTRICTED,
          userId: await AuthService.currentUserSub(),
        },
      }
    );
    return response;
  }

  static async listChannelMemberships(channelId: string) {
    const client = await this.getClient();
    const channelArn = this.getChannelArn(channelId);
    const response = await client
      .listChannelMemberships({
        ChannelArn: channelArn,
        ChimeBearer: await this.getChimeBearer(),
      })
      .promise();
    return response;
  }
  static async createChannelMembership(
    userArn: string,
    channelId: string,
    arn: boolean = false
  ) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.CREATE_CHANNEL_MEMBERSHIP;
    const response: CreateChannelMembershipResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          channelArn,
          memberArn: userArn,
          userId: await AuthService.currentUserSub(),
        },
      }
    );
    return response;
  }
  static async listChannels() {
    const path = API_ROUTES.LIST_USER_CHANNELS;
    const response: ListChannelMembershipsForAppInstanceUserResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          userId: await AuthService.currentUserSub(),
        },
      }
    );
    return response.ChannelMemberships?.map(
      (channel) => channel.ChannelSummary
    );
  }
  static async listChannelMessages(
    channelId: string,
    arn: boolean = false
  ): Promise<IListChannelMessageResponse> {
    const userChimeBearer = await this.getChimeBearer();
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.LIST_CHANNEL_MESSAGE;
    const response: ListChannelMessagesResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          channelArn,
          userId: await AuthService.currentUserSub(),
        },
      }
    );
    response.ChannelMessages = response.ChannelMessages ?? [];
    return response.ChannelMessages.map((message) => {
      return {
        Sender: message.Sender,
        Content: message.Content,
        CreatedTimestamp: message.CreatedTimestamp,
        LastUpdatedTimestamp: message.LastUpdatedTimestamp,
        MessageId: message.MessageId,
        Metadata: message.Metadata,
        Redacted: message.Redacted,
        IsSender: message.Sender?.Arn === userChimeBearer,
      };
    }).reduce(
      (acc, cur) => {
        if (!cur.MessageId) return acc;
        const messageVariantDetails = getMessageVariantDetails(cur);
        if (
          messageVariantDetails.AppMessageVariant === MESSAGE_VARIANT.STANDARD
        ) {
          acc.standard.push(cur);
        } else if (
          messageVariantDetails.AppMessageVariant ===
          MESSAGE_VARIANT.THREAD_MESSAGE
        ) {
          const replyMessageId = messageVariantDetails.ReplyMessageId as string;
          (acc.threadMessages[replyMessageId] =
            acc.threadMessages[replyMessageId] ?? []).push(cur);
        }
        return acc;
      },
      {
        threadMessages: {},
        standard: [],
      } as IListChannelMessageResponse
    );
  }
  static async channelInfo(channelId: string, arn: boolean = false) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.DESCRIBE_CHANNEL;
    const response: DescribeChannelResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          channelArn,
          userId: await AuthService.currentUserSub(),
        },
      }
    );
    return response;
  }
  static async listUsers() {
    const client = await this.getClient();
    const response = await client
      .listAppInstanceUsers({
        AppInstanceArn: APP_INSTANCE_ARN,
      })
      .promise();
    return response.AppInstanceUsers;
  }
  static getChannelArn(id: string) {
    return `${APP_INSTANCE_ARN}/channel/${id}`;
  }
  static async getChimeBearer() {
    if (!this.chimeBearer) {
      const userSub = await AuthService.currentUserSub();
      this.chimeBearer = `${APP_INSTANCE_ARN}/user/${userSub}`;
    }
    return this.chimeBearer;
  }
  static async getMessagingSessionEndpoint() {
    const path = API_ROUTES.GET_MESSAGING_SESSION_ENDPOINT;
    const response: GetMessagingSessionEndpointResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          userId: await AuthService.currentUserSub(),
        },
      }
    );
    return response;
  }
  static async getSession() {
    const chimeBearer = await this.getChimeBearer();
    const logger = new ConsoleLogger("SDK Chat Demo", LogLevel.INFO);
    const endpoint = await this.getMessagingSessionEndpoint();
    if (!endpoint?.Endpoint?.Url) return;
    const sessionConfig = new MessagingSessionConfiguration(
      chimeBearer,
      uuidv4(),
      endpoint.Endpoint.Url,
      await this.getClient(),
      AWS
    );
    const session = new DefaultMessagingSession(sessionConfig, logger);
    return session;
  }

  static async getDynamoDbClient() {
    const client = await this.getClient();
    AWS.config.update({
      region: "us-east-1",
      credentials: client.config.credentials,
    });
    const docClient = new AWS.DynamoDB.DocumentClient();
    return docClient;
  }

  static async sendThreadMessage(
    channelId: string,
    messageId: string,
    messageContent: string,
    arn: boolean = false
  ) {
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const path = API_ROUTES.SEND_THREAD_MESSAGE;
    const response: SendMessageResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          userId: await AuthService.currentUserSub(),
          messageContent,
          channelArn,
          persistence: ChannelMessagePersistenceType.PERSISTENT,
          replyMessageId: messageId,
        },
      }
    );
    return response;
  }

  static async deleteChannelMessages(channelId: string, arn: boolean = false) {
    // const chimeClient = await this.getClient();
    const channelArn = arn ? channelId : this.getChannelArn(channelId);
    const { Channel } = await this.channelInfo(channelArn, true);
    if (
      !Channel?.ChannelArn ||
      !Channel.Mode ||
      !Channel.Name ||
      !Channel.CreatedBy?.Arn
    )
      return;
    const metadata = JSON.parse(Channel.Metadata ?? "{}");
    metadata["MessageDeletedAt"] = new Date().valueOf();
    const path = API_ROUTES.UPDATE_CHANNEL;
    const response: UpdateChannelResponse = await API.post(
      environment.apiName,
      path,
      {
        body: {
          userId: await AuthService.currentUserSub(),
          channelArn,
          name: Channel.Name,
          mode: Channel.Mode,
          metadata: JSON.stringify(metadata),
        },
      }
    );
    return response;
  }
}
