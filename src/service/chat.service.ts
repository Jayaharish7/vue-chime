import { APP_CHANNEL_TYPE } from "@/constants/channel.constants";
import { IChannelMessage } from "@/types/channel.types";
import { ChannelPrivacy } from "@aws-sdk/client-chime";
import { Message, MessagingSessionObserver } from "amazon-chime-sdk-js";
import { ChimeService } from "./chime.service";
import { v4 as uuidv4 } from "uuid";

export class ChatService {
  static messageCallbacks: {
    [key: string]: (message: Message) => any | undefined;
  } = {};
  static addListener(fn: (message: Message) => any | undefined) {
    const key = uuidv4();
    this.messageCallbacks[key] = fn;
    return key;
  }
  static async listen() {
    const messageObserver: MessagingSessionObserver = {
      messagingSessionDidStart: () => {},
      messagingSessionDidStartConnecting: (reconnecting: any) => {},
      messagingSessionDidStop: (event: any) => {},
      messagingSessionDidReceiveMessage: (message: Message) =>
        this.publishMessage(message),
    };
    const session = await ChimeService.getSession();
    session?.addObserver(messageObserver);
    session?.start();
  }
  static publishMessage(message: Message) {
    Object.values(this.messageCallbacks).forEach((cb) => cb?.(message));
  }
  static removeListener(key: string) {
    delete this.messageCallbacks[key];
  }
}
