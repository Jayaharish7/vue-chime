import { createApp } from "vue";
import App from "./App.vue";

import { API, Auth } from "aws-amplify";

import "./styles/app.css";
import { AWS_EXPORTS } from "./aws-exports";

import { Router } from "./routes/router";

Auth.configure(AWS_EXPORTS);
API.configure(AWS_EXPORTS)

const app = createApp(App);
app.use(Router);
app.mount("#app");
