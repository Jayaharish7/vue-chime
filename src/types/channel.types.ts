import {
  ChannelMessagePersistenceType,
  ChannelMessageSummary,
  ChannelMessageType,
  ChannelPrivacy,
} from "@aws-sdk/client-chime";
import { ChannelMode } from "aws-sdk/clients/chime";

export interface IChannelMessage extends ChannelMessageSummary {
  IsSender: boolean;
}

export type TChannelMessages = {
  [key: string]: IChannelMessage[];
};

export interface ICreateMessageEventPayload {
  ChannelArn: string;
  Content: string;
  CreatedTimestamp: string;
  LastUpdatedTimestamp: string;
  LastEditedTimestamp: string;
  MessageId: string;
  Persistence: ChannelMessagePersistenceType;
  Redacted: false;
  Sender: { Name: string; Arn: string };
  Type: ChannelMessageType;
  Metadata?: string;
}
export interface IUpdateMessageEventPayload {
  ChannelArn: string;
  Content: string;
  CreatedTimestamp: string;
  LastEditedTimestamp: string;
  LastUpdatedTimestamp: string;
  MessageId: string;
  Metadata: string;
  Persistence: ChannelMessagePersistenceType;
  Redacted: boolean;
  Sender: { Name: string; Arn: string };
  Type: ChannelMessageType;
}

export interface IUpdateChannelEventPayload {
  ChannelArn: string;
  CreatedBy: {
    Arn: string;
    Name: string;
  };
  CreatedTimestamp: string;
  LastUpdatedTimestamp: string;
  Metadata: string; //"{\"MessageDeletedAt\":1632410944169}"
  Mode: ChannelMode;
  Name: string;
  Privacy: ChannelPrivacy;
}

export interface IListChannelMessageResponse {
  standard: IChannelMessage[],
  threadMessages: {
    [key: string]: IChannelMessage[]
  }
}